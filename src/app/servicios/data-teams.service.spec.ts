import { TestBed } from '@angular/core/testing';

import { DataTeamsService } from './data-teams.service';

describe('DataTeamsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataTeamsService = TestBed.get(DataTeamsService);
    expect(service).toBeTruthy();
  });
});
