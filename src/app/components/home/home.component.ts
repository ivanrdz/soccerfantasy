import { Component, OnInit } from '@angular/core';
import { DataTeamsService, Teams } from 'src/app/servicios/data-teams.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  teams: Teams[] = [];

  constructor( private _dataTeamsService: DataTeamsService ) {
    console.log('constructr de heroes');
  }

  ngOnInit() {
    this.teams = this._dataTeamsService.getTeams();
    console.log(this.teams);
  }

}
